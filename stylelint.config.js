/**
 * Stylelint configuration for comdirect project
 *
 * Labels for disabled rules can be used as identifier for W3C spec as follows:
 * https://www.w3.org/TR/<label>/ e.g. https://www.w3.org/TR/css-color-3
 */

module.exports = {
  extends: 'stylelint-config-standard',
  rules: {
    indentation: 'tab',
    // Recommended when autoprefixer is active
    // 'at-rule-no-vendor-prefix': true,
    'selector-max-compound-selectors': 7,
    // Allowing 4 levels of nesting, borrowed from 'Inception Rule'
    'max-nesting-depth': [7, {
      ignore: ['at-rules-without-declaration-blocks'],
    }],
    // Disabling LESS functions among others
    // LESS function reference: https://github.com/less/less.js/tree/master/lib/less/functions
    'function-blacklist': [
      /*
      * Color functions
      */
      // 'rgb', // [css-color-3]
      // 'rgba', // [css-color-3]
      // 'hsl', // [css-color-3]
      // 'hsla', // [css-color-3]
      'hsv',
      'hsva',
      'hue',
      'saturation',
      'lightness',
      'hsvhue',
      'hsvsaturation',
      'hsvvalue',
      'red',
      'green',
      'blue',
      'alpha',
      'luma',
      'luminance',
      // 'saturate', // [filter-effects]
      'desaturate',
      'lighten',
      'darken',
      'fadein',
      'fadeout',
      'spin',
      'mix',
      // 'greyscale', // [filter-effects]
      // 'contrast', // [filter-effects]
      'argb',
      'color',
      'tint',
      'shade',

      /*
      * Color blend functions
      */
      'multiply',
      'screen',
      'overlay',
      'softlight',
      'hardlight',
      'difference',
      'exclusion',
      'average',
      'negation',

      /*
      * String functions
      */
      'e',
      'escape',
      'replace',
      '%',

      /*
      * Number functions
      */
      'min',
      'max',
      'convert',
      'pi',
      'mod',
      'pow',
      'percentage',

      /*
      * Math functions
      */
      'ceil',
      'floor',
      'round',
      'sqrt',
      'abs',
      'tan',
      'sin',
      'cos',
      'atan',
      'asin',
      'acos',

      /*
      * Type functions
      */
      'isruleset',
      'iscolor',
      'isnumber',
      'isstring',
      'iskeyword',
      'isurl',
      'ispixel',
      'ispercentage',
      'isem',
      'isunit',
      'unit',
      'extract',
      'length',

      /*
      * SVG functions
      */
      'svg-gradient',

      /*
      * Data-uri functions
      */
      'data-uri',
    ],
  },
};
