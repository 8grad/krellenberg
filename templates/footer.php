<footer class="container">
  <div class="row">
    <div class="col-sm-12">
      <small>
        <p><?= '© ' . date( 'Y' ) ?> <?= bloginfo( 'name' ) ?></p>
      </small>
      <?php edit_post_link( '🔏', '<p>', '</p>' ); ?>
      <?php dynamic_sidebar( 'sidebar-footer' ); ?>
    </div>
  </div>
</footer>
