<?php //dynamic_sidebar('sidebar-primary'); ?>
<div class="left">
  <?php
  if ( has_nav_menu( 'secondary_navigation' ) ) :
    wp_nav_menu( [ 'theme_location' => 'secondary_navigation', 'menu_class' => '' ] );
  endif;
  ?>
</div>

<div class="right">
  <a class="brand" href="<?= esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
</div>
