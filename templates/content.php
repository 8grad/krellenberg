<?php
namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

$id        = get_the_ID();
$videoLink = get_post_custom_values( "vimeo-video-link", $id );

if ( isset( $videoLink[0] ) && $videoLink[0] != "" ) {
	$classes         = "popup-vimeo";
	$playbuttonClass = "play";
} else {
	$classes         = "";
	$playbuttonClass = "";
}
?>


<article <?php post_class(); ?>>
    <section class="entry-visual <?= $playbuttonClass ?>">
		<?php

		if ( isset( $videoLink[0] ) && $videoLink[0] != "" ) {
			$urlParts = explode( "/", parse_url( $videoLink[0], PHP_URL_PATH ) );
			$videoId  = (int) $urlParts[ count( $urlParts ) - 1 ];

			// if we have an resulting ID (int() pass it to the function)
			if ( $videoId ) {
				echo getVimeoThumb( $videoId );
			} else {
				the_postImageWithoutSizes();
			}

		} else {
			the_postImageWithoutSizes();
		}


		?>
		<?php
		if ( isset( $videoLink[0] ) && $videoLink[0] != "" ) {
			?>
            <nav class="videoplayer-play"></nav><?php
		}
		?>
    </section>

    <section class="entry-info">
        <header>
            <h2 class="entry-title"><?php the_title(); ?></h2>
            <h2 class="entry-title">
				<?php

				if ( isset( $videoLink[0] ) ) {
					?>
                    <a class="<?php echo $classes ?>" href="<?php echo $videoLink[0] ?>"><?php the_title(); ?></a>
					<?php
				} else {
					?>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<?php } ?>
            </h2>
			<?php get_template_part( 'templates/entry-meta' ); ?>
        </header>
        <div class="entry-summary hidden ">
			<?php the_excerpt(); ?>
        </div>
    </section>

</article>
