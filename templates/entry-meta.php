
<?php
$photos = get_post_meta($post->ID, 'photo', true);
$collection = get_post_meta($post->ID, 'collection', true);
?>

<?php
  if(isset($photos) && $photos !='') {
    ?>
      <p class="photos"><?php echo $photos ?> </p>
    <?php
  }

  if(isset($collection) && $collection !='') {
    ?>
    <p class="collection"><?php echo $collection ?></p>
    <?php
  }
?>
