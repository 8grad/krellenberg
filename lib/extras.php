<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class( $classes ) {
  // Add page slug if it doesn't exist
  if ( is_single() || is_page() && ! is_front_page() ) {
    if ( ! in_array( basename( get_permalink() ), $classes ) ) {
      $classes[] = basename( get_permalink() );
    }
  }

  // Add class if sidebar is active
  if ( Setup\display_sidebar() ) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}

add_filter( 'body_class', __NAMESPACE__ . '\\body_class' );

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __( 'Continued', 'sage' ) . '</a>';
}

add_filter( 'excerpt_more', __NAMESPACE__ . '\\excerpt_more' );


/**
 * Clean up gallery_shortcode()
 *
 * Re-create the [gallery] shortcode and use thumbnails styling from Bootstrap
 * The number of columns must be a factor of 12.
 *
 * @link http://getbootstrap.com/components/#thumbnails
 */
function roots_gallery( $attr ) {
  $post = get_post();

  static $instance = 0;
  $instance ++;

  if ( ! empty( $attr['ids'] ) ) {
    if ( empty( $attr['orderby'] ) ) {
      $attr['orderby'] = 'post__in';
    }
    $attr['include'] = $attr['ids'];
  }

  $output = apply_filters( 'post_gallery', '', $attr );

  if ( $output != '' ) {
    return $output;
  }

  if ( isset( $attr['orderby'] ) ) {
    $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
    if ( ! $attr['orderby'] ) {
      unset( $attr['orderby'] );
    }
  }

  extract( shortcode_atts( array(
    'order'      => 'ASC',
    'orderby'    => 'menu_order ID',
    'id'         => $post->ID,
    'itemtag'    => '',
    'icontag'    => '',
    'captiontag' => '',
    'columns'    => 4,
    'size'       => 'thumbnail',
    'include'    => '',
    'exclude'    => '',
    'link'       => ''
  ), $attr ) );

  $id      = intval( $id );
  $columns = ( 12 % $columns == 0 ) ? $columns : 4;
  $grid    = sprintf( 'col-sm-%1$s col-lg-%1$s', 12 / $columns );

  if ( $order === 'RAND' ) {
    $orderby = 'none';
  }

  if ( ! empty( $include ) ) {
    $_attachments = get_posts( array(
      'include'        => $include,
      'post_status'    => 'inherit',
      'post_type'      => 'attachment',
      'post_mime_type' => 'image',
      'order'          => $order,
      'orderby'        => $orderby
    ) );

    $attachments = array();
    foreach ( $_attachments as $key => $val ) {
      $attachments[ $val->ID ] = $_attachments[ $key ];
    }
  } elseif ( ! empty( $exclude ) ) {
    $attachments = get_children( array(
      'post_parent'    => $id,
      'exclude'        => $exclude,
      'post_status'    => 'inherit',
      'post_type'      => 'attachment',
      'post_mime_type' => 'image',
      'order'          => $order,
      'orderby'        => $orderby
    ) );
  } else {
    $attachments = get_children( array(
      'post_parent'    => $id,
      'post_status'    => 'inherit',
      'post_type'      => 'attachment',
      'post_mime_type' => 'image',
      'order'          => $order,
      'orderby'        => $orderby
    ) );
  }

  if ( empty( $attachments ) ) {
    return '';
  }

  if ( is_feed() ) {
    $output = "\n";
    foreach ( $attachments as $att_id => $attachment ) {
      $output .= wp_get_attachment_link( $att_id, $size, true ) . "\n";
    }

    return $output;
  }

  $unique = ( get_query_var( 'page' ) ) ? $instance . '-p' . get_query_var( 'page' ) : $instance;

  $output = "";
  $output .= '<section class="swiper-container">

        <!-- Additional required wrapper -->
        <ul class="swiper-wrapper">';


  foreach ( $attachments as $id => $attachment ) {

    switch ( $link ) {
      case 'file':
        $imgSrc  = wp_get_attachment_image_src( $id, "full" );
        $imgIcon = wp_get_attachment_image_src( $id, "thumbnail" );
        break;
      case 'none':
        $imgSrc  = wp_get_attachment_image_src( $id, "full" );
        $imgIcon = wp_get_attachment_image_src( $id, "thumbnail" );
        break;
      default:
        $imgSrc  = wp_get_attachment_image_src( $id, "full" );
        $imgIcon = wp_get_attachment_image_src( $id, "thumbnail" );
        break;
    }

    $ratio = 0;
    $ratio = floatval( round( $imgSrc[1] / $imgSrc[2], 1 ) );

    switch ( $ratio ) {
      case $ratio < 1:
        $aspectRatio = "portrait";
        break;

      case $ratio > 1:
        $aspectRatio = "landscape";
        break;

      default:
        break;
    }

    $output .= "<li class=\"swiper-slide $aspectRatio\" data-hash=\"krellenberg-$id\" >";
    $output .= "<img data-ratio=\"$ratio\" class=\"isLoading $aspectRatio\" src=\"$imgIcon[0]\" alt=\"© Kai Krellenberg\" data-src=\"$imgSrc[0]\">";
    $output .= "<!-- Preloader image -->";
    $output .= "<div class=\"swiper-lazy-preloader swiper-lazy-preloader-black\"></div>";
    $output .= "</li>";
  }
  $output .= "</ul>
        <!-- Add Scrollbar -->
        <div class=\"swiper-scrollbar\"></div>

        <!-- Add Pagination -->
        <ul class=\"swiper-pagination\"></ul>

        <!-- Add Arrows -->
        <div class=\"swiper-button-next swiper-button-black\"></div>
        <div class=\"swiper-button-prev swiper-button-black\"></div>

        </section>";

  return $output;
}

remove_shortcode( 'gallery' );
add_shortcode( 'gallery', __NAMESPACE__ . '\\roots_gallery' );
add_filter( 'use_default_gallery_style', __NAMESPACE__ . '\\__return_null' );


function the_postImageWithoutSizes() {
  $post  = get_post();
  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

  if ( $image ) :
    echo "<img src='" . $image[0] . "' alt='' />";
  endif;

}


function getVimeoThumb( $id ) {
  $vimeo = unserialize( file_get_contents( "https://vimeo.com/api/v2/video/$id.php" ) );

  echo "<img src='" . str_replace( 'http://', 'https://', $vimeo[0]['thumbnail_large'] ) . "' alt='' />";
}


// custom file rename function;
add_filter( 'mfrh_new_filename', __NAMESPACE__ . '\\my_filter_filename', 10, 3 );
function my_filter_filename( $new, $old, $post ) {
  return "kai-krellenberg__" . $new;
}


// Get menus to play nicely with the submenu script
// blissfully borrowed from Post Type Archive Links plugin, thanks @stephenharris, @F J Kaiser, @ryancurban
function sage_tax_archive_current( $items ) {
  foreach ( $items as $item ) {
    if ( 'taxonomy' !== $item->type ) {
      continue;
    }

    global $post;

    if ( ! $post ) {
      continue;
    }

    if ( is_home() ) {
      continue;
    }

    $taxonomy      = $item->object;
    $taxonomy_term = $item->object_id;
    if (
      ! is_tax( $taxonomy, $taxonomy_term )
      and ! has_term( $taxonomy_term, $taxonomy, $post->ID )
    ) {
      continue;
    }

    // Make item current
    $item->current   = true;
    $item->classes[] = 'current-menu-item';

    // Loop through ancestors and give them 'parent' or 'ancestor' class
    $active_anc_item_ids = sage_get_item_ancestors( $item );
    foreach ( $items as $key => $parent_item ) {
      $classes = (array) $parent_item->classes;

      // If menu item is the parent
      if ( $parent_item->db_id == $item->menu_item_parent ) {
        $classes[]                          = 'current-menu-parent';
        $items[ $key ]->current_item_parent = true;
      }

      // If menu item is an ancestor
      if ( in_array( intval( $parent_item->db_id ), $active_anc_item_ids ) ) {
        $classes[]                            = 'current-menu-ancestor';
        $items[ $key ]->current_item_ancestor = true;
      }

      $items[ $key ]->classes = array_unique( $classes );
    }
  }

  return $items;
}

add_filter( 'wp_nav_menu_objects', __NAMESPACE__ . '\\sage_tax_archive_current' );

function sage_get_item_ancestors( $item ) {
  $anc_id = absint( $item->db_id );

  $active_anc_item_ids = array();
  while (
    $anc_id = get_post_meta( $anc_id, '_menu_item_menu_item_parent', true )
    and ! in_array( $anc_id, $active_anc_item_ids )
  ) {
    $active_anc_item_ids[] = $anc_id;
  }

  return $active_anc_item_ids;
}


// disable emoji stuff
function disable_wp_emojicons() {
  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );


  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', __NAMESPACE__ . '\\disable_emojicons_tinymce' );
}

add_action( 'init', __NAMESPACE__ . '\\disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

add_filter( 'emoji_svg_url', '__return_false' );

// remove generator
remove_action( 'wp_head', 'wp_generator' );

// remove jquery migrate
add_action( 'wp_default_scripts', function ( $scripts ) {
  if ( ! empty( $scripts->registered['jquery'] ) ) {
    $scripts->registered['jquery']->deps = array_diff( $scripts->registered['jquery']->deps, array( 'jquery-migrate' ) );
  }
} );

/** * Completely Remove jQuery From WordPress Admin Dashboard */
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\no_more_jquery' );

function no_more_jquery() {
  //wp_deregister_script('jquery');
  /** * Install latest jQuery version 3.5.1. */
  if ( ! is_admin() ) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', ( "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" ), false );
    wp_enqueue_script( 'jquery' );
  }
}


