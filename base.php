<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
<?php get_template_part( 'templates/head' ); ?>
<body <?php body_class(); ?>>
<!--[if IE]>
<div class="alert alert-warning">
  <?php _e( 'You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your
  browser</a> to improve your experience.', 'sage' ); ?>
</div>
<![endif]-->

<div class="site--wrapper">
  <div class="site">
    <?php
    do_action( 'get_header' );
    get_template_part( 'templates/header' );
    ?>

    <div class="wrap container" role="document">
      <div class="content row">
        <?php if ( is_home() || is_category() || is_page() || is_404() ) : ?>
          <main class="col-sm-12 content main">
            <?php include Wrapper\template_path(); ?>
          </main>
        <?php endif; ?>

        <?php if ( Setup\display_sidebar() ) : ?>
          <nav class="col-sm-12 navigation">

            <?php include Wrapper\sidebar_path(); ?>
          </nav><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div>

    <!-- full width slider template -->
    <?php if ( ! is_home() && ! is_page() && ! is_category() && ! is_404() ) : ?>
      <div class="wrap container-fluid">
        <div class="content row">
          <main class="content main main--wide">
            <?php include Wrapper\template_path(); ?>
          </main>
        </div>
      </div>
    <?php endif; ?>
    <?php
    do_action( 'get_footer' );
    get_template_part( 'templates/footer' );
    wp_footer();
    ?>
  </div>
</div>
</body>
</html>
