"use strict";

/**
 * @constructor
 */

function ThemeMenu(element, options) {
  var site = {};
  var self = this;
  var body = jQuery('body');
  self.$el = jQuery(element);
  self.options = jQuery.extend({}, ThemeMenu.DEFAULTS, options);

  self.$menu = null;
  self.menuIsOpen = false;

  function init() {
    site.siteMenu = self;
    self.$menu = jQuery('.menu-link', self.$el);
    self.$panel = self.$menu.next();

    // init menu
    self.$menu.bigSlide({
      "speed": 200,
      "menuWidth": "220px",
      "easyClose": true,
      'beforeOpen': function () {
        self.$panel.addClass('visible');
        self.menuIsOpen = false;
      },
      'afterOpen': function () {
        self.menuIsOpen = true;
        body.css('overflow', 'hidden');
      },
      'afterClose': function () {
        body.css('overflow', 'auto');
      }
    });

  }

  init();
}

ThemeMenu.DEFAULTS = {};

ThemeMenu.AutoInit = function () {
  jQuery('body').each(function () {
    var el = jQuery(this);
    var obj = el.data('theme.menu');
    var data = el.data();

    if (!obj) {
      el.data('theme.menu', (obj = new ThemeMenu(this, data)));
    }
  });
};
