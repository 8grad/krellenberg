"use strict";

/**
 * @constructor
 */

function ThemeSwiper(element, options) {
  var self = this;
  self.$el = jQuery(element);
  self.options = jQuery.extend({}, ThemeSwiper.DEFAULTS, options);

  self.$slideItems = null;
  self.$sliderContainer = null;
  self.$contentContainer = null;
  self.slider = null;

  function init() {
    /* set up targets */
    self.$sliderContainer = jQuery('.swiper-container', self.$el);

    var $slidee = self.$sliderContainer.find(".swiper-wrapper");
    var prevClass = '.swiper-button-prev';
    var nextClass = '.swiper-button-next';
    var pagesClass = '.swiper-pagination';
    var scrollbarClass = '.swiper-scrollbar';

    var isLoadingClass = 'isLoading';

    function betterLazyLoader() {
      $slidee.find('li img').each(function () {
        var $img = jQuery(this);
        var $imgSrc = jQuery(this).attr('data-src');

        var img = new Image();

        img.onload = function () {
          // image is loaded...
          $img.removeClass(isLoadingClass);
          $img.next('.swiper-lazy-preloader').remove();
        };

        $img.addClass(isLoadingClass);
        $img.attr('src', $imgSrc);

        // The onload function will now trigger once it's loaded.
        img.src = $imgSrc;
      });
    }


    var mySwiper = new Swiper(self.$sliderContainer, { // jshint ignore:line
      // Autoplay
      autoplay: false, //MS == 2 min.
      autoplayDisableOnInteraction: true,

      //Loop ~ infinite
      loop: false,

      // Scrollbar
      //scrollbar: scrollbarClass,
      //scrollbarDraggable: true,
      //scrollbarHide: false,
      //scrollbarSnapOnRelease: true,

      centeredSlides: false,
      grabCursor: true,

      // Pagination
      pagination: pagesClass,
      paginationClickable: true,

      // Keyboard
      keyboardControl: true,
      nextButton: nextClass,
      prevButton: prevClass,

      // Mousewheel
      mousewheelControl: false,

      // Hash Navigation
      hashnav: true,
      hashnavWatchState: true,

      // No Fixed Position
      freeMode: false,

      onImagesReady: betterLazyLoader(),

      // Zoom
      zoom: true,

      slidesPerView: '1',
      spaceBetween: 0
    });


  }

  /* hover fix */
  var mobileHover = function () {
    jQuery('*').on('touchstart', function () {
      jQuery(this).trigger('hover');
    }).on('touchend', function () {
      jQuery(this).trigger('hover');
    });
  };

  mobileHover();

  init();

}

ThemeSwiper.DEFAULTS = {};

ThemeSwiper.AutoInit = function () {
  jQuery('body').each(function () {
    var el = jQuery(this);
    var obj = el.data('theme.touch.slider');
    var data = el.data();

    if (!obj) {
      el.data('theme.touch.slider', (obj = new ThemeSwiper(this, data)));
    }

  });
};

