<?php get_template_part( 'templates/page', 'header' ); ?>


<?php if ( ! have_posts() ) : ?>
  <div class="col-span-12">
    <div class="alert alert-warning">
      <?php _e( 'Sorry, no results were found.', 'sage' ); ?>
    </div>
    <?php get_search_form(); ?>

  </div>
<?php endif; ?>


<?php if ( ! is_home() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <?php get_template_part( 'templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format() ); ?>
  <?php endwhile; ?>

<?php else: ?>

  <?php $query = new WP_Query( 'cat=-5' ); ?>

  <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
    <?php get_template_part( 'templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format() ); ?>
  <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
  <?php endif; ?>

<?php endif; ?>

<?php the_posts_navigation(); ?>
