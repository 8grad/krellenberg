<?php get_template_part('templates/page', 'header'); ?>

<div class="col-span-12">
  <div class="alert alert-warning">
    <?php _e('uups… 404, the page does not exist', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>

</div>

<?php the_posts_navigation(); ?>
